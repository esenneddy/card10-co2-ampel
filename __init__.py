"""
co2-ampel
===========
This app shows the current equivalent CO2 value and displays a led warning if above certain thresholds.
"""
import buttons
import color
import display
import os
import time
import bme680
import leds
import color
import light_sensor

threshold_1 = 1000 # Threshold green->yellow
threshold_2 = 2000 # threshold yellow->red
update_interval = 3 # The bsec driver has a fixed update interval of 3s so it makes no sense to wake up earlier.

def dimmer():
    helligkeit = light_sensor.get_reading()
    # In my experiments, the light_sensor reading was between 10 (darkness) and 21 (shone upon with a LED flashlight).
    if helligkeit < 10:
        dimwert = 1
    elif helligkeit > 20:
        dimwert = 8
    else:
        dimwert = (7 * (helligkeit-10) // 10) + 1
    return dimwert

def main():
    disp = display.open()
    light_sensor.start()
    disp.clear().update()
    leds.clear()
    with bme680.Bme680() as environment:
        while True:
            data = environment.get_data()

            leds.dim_bottom(dimmer())            
            if data.iaq_accuracy > 1:
                if data.eco2 < threshold_1:
                    c=color.GREEN
                elif data.eco2 < threshold_2:
                    c=color.YELLOW
                else:
                    c=color.RED
                leds.set(leds.BOTTOM_LEFT, c)
            else:
                c=color.WHITE
                leds.clear()
                
            disp.clear()
            disp.print("CO2 (ppm)", posx=0, posy=0, font=1)
            disp.print("Accuracy: {:1.0f}".format(data.iaq_accuracy), posx=0, posy=65, font=1)
            disp.print("{:7.2f}".format(data.eco2), posx=10, posy=30, font=4, fg=c)
            disp.update()
        
            time.sleep(update_interval)


if __name__ == "__main__":
    main()
