# card10-CO2-Ampel

CO2-Ampel für das card10-Badge des CCCamp 2019

## User guide
Just start the app. Its only screen shows the calculated equivalent CO2 value based on VOC measurement. In the bottom of the screen, it shows the calculated [accuracy](https://firmware.card10.badge.events.ccc.de/epicardium/api.html#bsec) of the BSEC driver. Values of 0 or 1 should be considered unreliable.  
When the accuracy is 2 or 3, the eCO2 value shows in green unter 1000 ppm, in yellow between 1000 and 2000 and in red above. Also an LED ls lit in the same colour.

## Remarks

* The BME680 sensor does not measure CO2 itself but calculates the equivalent CO2 value (eCO2) out of measurements of volatile organic compounds (VOC).
* The app not work with stock firmware 1.16. I made it work with firmware built from sources on 2021-01-03. Should work with stock 1.17 according to the [card10 documentation](https://firmware.card10.badge.events.ccc.de/pycardium/bme680.html).
* Enabling the proprietary BSEC driver requires a card10.cfg file in the root directory of card10's file system with the line
    bsec_enable = true
* For sensible CO2 thresholds, I found the following German language source: [Umweltbundesamt – Gesundheitliche Bewertung von Kohlendioxid in der  Innenraumluft](https://www.umweltbundesamt.de/sites/default/files/medien/pdfs/kohlendioxid_2008.pdf). Based on that, I chose the thresholds 1000ppm (green->yellow) and 2000ppm (yellow->red).
